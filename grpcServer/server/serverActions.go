package server

import (
	"github.com/boltdb/bolt"
	"time"
	"golang.org/x/net/context"
	mod "gitlab.com/oleg.kuleba/goGRPC/models"
	"net"
	"log"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc"
	"github.com/golang/protobuf/proto"
)

const DBName string = "phoneBook.db"
const ContactsBucketName string = "contacts"
const MsgContactAlreadyExist = "Запись с таким номером уже существует. Для изменения данных используйте команду editContact"
const MsgContactNonExist = "Запись с таким номером не существует"
const MsgEmptyAddressBook = "Записи в базе данных пока отсутствуют. Можете добавить запись с помощью команды addContact"

type server struct{}

func Start() {
	lis, err := net.Listen("tcp", ":50051")
	Check(err)
	serv := grpc.NewServer()
	mod.RegisterAddressBookServer(serv, &server{})
	reflection.Register(serv)
	if err := serv.Serve(lis); err != nil {
		log.Fatal("server.Serve(listener) was failed")
	}
}

func (s *server) AddContact(ctx context.Context, in *mod.ContactRequestOrResponse) (*mod.BoolMessageReply, error) {
	// Открываем БД
	db, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer db.Close()

	response := &mod.BoolMessageReply{}

	// Открываем Writable транзакцию
	if err := db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(ContactsBucketName))
		Check(err)

		if bucket.Get([]byte(in.Phone.Number)) != nil { // Проверяем наличие номера в БД (для обеспечения уникальности). Если есть - сообщаем об этом и выходим
			response.IsOk = false
			response.Msg = MsgContactAlreadyExist
		} else { // Если номера в БД нет - добавляем
			contact, err := proto.Marshal(in.Contact)
			Check(err)
			err = bucket.Put([]byte(in.Phone.Number), contact)
			Check(err)
			response.IsOk = true
		}
		return nil
	}); err != nil {
		panic(err)
	}
	return response, nil
}

func (s *server) FindAll(in *mod.Empty, stream mod.AddressBook_FindAllServer) error {

	// Открываем БД
	db, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer db.Close()

	// Открываем Read-only транзакцию
	if err := db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(ContactsBucketName))
		var cursor *bolt.Cursor

		if bucket == nil { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			//setEmptyBucketMessage()
		} else {
			contactResp := &mod.ContactRequestOrResponse{}
			cursor = bucket.Cursor()
			if k, _ := cursor.First(); k == nil { // Если cursor баккета пустой (ключей/записей нет) - сообщаем об этом и выходим
				//setEmptyBucketMessage()
				return nil
			}
			// Считываем контакты и отправлем в потоке на клиент
			for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
				contact := &mod.Contact{}
				err := proto.Unmarshal(v, contact)
				Check(err)

				// Записываем номер и контакт в структуру ContactRequestOrResponse{} и отправляем на клиент
				contactResp.Contact = contact
				contactResp.Phone = &mod.Phone{Number: string(k)}
				if err := stream.Send(contactResp); err != nil {
					return err
				}
			}
		}
		return nil
	}); err != nil {
		panic(err)
	}
	return nil
}

func (s *server) FindByNumber(ctx context.Context, in *mod.Phone) (*mod.ContactRequestOrResponse, error) {
	// Открываем БД
	db, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 5 * time.Second})
	Check(err)
	defer db.Close()

	contactResp := &mod.ContactRequestOrResponse{}

	// Открываем Read-only транзакцию
	if err := db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(ContactsBucketName))
		if bucket == nil { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			//setEmptyBucketMessage()
		} else {
			if contactBytes := bucket.Get([]byte(in.Number)); contactBytes != nil { // Если номер есть в БД - выводим инфу о нем на дисплей

				contact := &mod.Contact{}
				err := proto.Unmarshal(contactBytes, contact)
				Check(err)

				contactResp.Phone = in
				contactResp.Contact = contact
			}
		}
		return nil
	}); err != nil {
		panic(err)
	}
	return contactResp, nil
}

func (s *server) EditContact(ctx context.Context, in *mod.ContactRequestOrResponse) (*mod.BoolMessageReply, error) {
	db, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer db.Close()

	response := &mod.BoolMessageReply{}

	// Открываем Writable транзакцию
	if err := db.Update(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactFromDB := bucket.Get([]byte(in.Phone.Number)); contactFromDB != nil { // Проверяем наличие номера в БД
				contactToDB, err := proto.Marshal(in.Contact)
				Check(err)
				err = bucket.Put([]byte(in.Phone.Number), contactToDB) // Перезаписываем контакт в БД
				Check(err)
				response.IsOk = true
			} else { // Если номера нет в БД - сообщаем об этом
				response.IsOk = false
				response.Msg = MsgContactNonExist
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом
			response.IsOk = false
			response.Msg = MsgEmptyAddressBook
		}

		return nil
	}); err != nil {
		panic(err)
	}
	return response, nil
}

func (s *server) DeleteContact(ctx context.Context, phone *mod.Phone) (*mod.BoolMessageReply, error) {

	// Открываем БД
	dbContacts, err := bolt.Open(DBName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	Check(err)
	defer dbContacts.Close()

	response := &mod.BoolMessageReply{}

	// Открываем Writable транзакцию
	if err := dbContacts.Update(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contactTmp := bucket.Get([]byte(phone.Number)); contactTmp != nil { // Проверяем наличие номера в БД
				contactFromDB := &mod.Contact{}
				err := proto.Unmarshal(contactTmp, contactFromDB)
				Check(err)

				err = bucket.Delete([]byte(phone.Number)) // Удаляем запись с БД
				Check(err)
				response.IsOk = true
			} else { // Если номера нет в БД - сообщаем об этом и выходим
				response.IsOk = false
				response.Msg = MsgContactNonExist
			}
		} else { // Если bucket не существует (совсем нет записей) - сообщаем об этом и выходим
			response.IsOk = false
			response.Msg = MsgEmptyAddressBook
		}
		return nil
	}); err != nil {
		panic(err)
	}

	return response, nil
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
