package client

import (
	"fmt"
	"regexp"
	mod "gitlab.com/oleg.kuleba/goGRPC/models"
	"google.golang.org/grpc"
	"golang.org/x/net/context"
	"io"
)

const ServerAddress string = "localhost:50051"
const DBName string = "phoneBook.db"
const ContactsBucketName string = "contacts"

// Флаги, использующиеся для валидации
const PhoneFlag string = "phone"
const NameFlag string = "name"
const CityFlag string = "city"
const StreetFlag string = "street"
const BuildingOrApartmentFlag string = "buildingOrApartment"

var PhoneRegexp *regexp.Regexp = regexp.MustCompile("^[+]380([0-9]{9})$")
var NameRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{3,15}$")
var CityRegexp *regexp.Regexp = regexp.MustCompile("^[A-Za-z]{3,15}$")
var StreetRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{4,20}$")
var BuildingOrApartmentRegexp *regexp.Regexp = regexp.MustCompile("^[0-9A-Za-z]{1,5}$")

func getConnAndClient(address string, opt grpc.DialOption) (*grpc.ClientConn, mod.AddressBookClient) {
	conn, err := grpc.Dial(ServerAddress, grpc.WithInsecure())
	Check(err)
	client := mod.NewAddressBookClient(conn)
	return conn, client
}

func AddContact(number string, contact *mod.Contact) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &mod.Phone{
		Number: number,
	}
	contactRequest := &mod.ContactRequestOrResponse{}
	contactRequest.Contact = contact
	contactRequest.Phone = phone

	res, err := client.AddContact(context.Background(), contactRequest)
	Check(err)
	return res.IsOk, res.Msg
}

func FindAll() {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()
	stream, err := client.FindAll(context.Background(), &mod.Empty{})
	Check(err)
	for {
		contactResponse, err := stream.Recv()
		if err == io.EOF {
			break
		}
		Check(err)
		PrintContact(contactResponse.Phone.Number, contactResponse.Contact)
	}
}

func FindByNumber(number string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()
	numberRequest := &mod.Phone{
		Number: number,
	}
	contactResponse, err := client.FindByNumber(context.Background(), numberRequest)
	Check(err)

	if contactResponse.Contact != nil {
		contact := contactResponse.Contact
		Check(err)
		fmt.Println("Найдена запись:")
		PrintContact(number, contact)
	} else { // Если номера нет в БД - сообщаем об этом
		fmt.Println("Запись с таким номером не существует")
	}
}

func EditContact(number string, contact *mod.Contact) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &mod.Phone{
		Number: number,
	}
	contactRequest := &mod.ContactRequestOrResponse{}
	//
	contactRequest.Phone = phone
	contactRequest.Contact = contact

	res, err := client.EditContact(context.Background(), contactRequest)
	Check(err)
	return res.IsOk, res.Msg
}

func DeleteContact(number string) (bool, string) {
	conn, client := getConnAndClient(ServerAddress, grpc.WithInsecure())
	defer conn.Close()

	phone := &mod.Phone{
		Number: number,
	}

	res, err := client.DeleteContact(context.Background(), phone)
	Check(err)
	return res.IsOk, res.Msg
}

func printEmptyBucketMessage() {
	fmt.Println("Записи в базе данных пока отсутствуют. Можете добавить запись с помощью команды addContact")
}

func PrintContact(phone string, contact *mod.Contact) {
	fmt.Println("contact {")
	fmt.Println("PHONE:", phone)
	fmt.Println("NAME:", contact.Name)
	fmt.Println("ADDRESS:", contact.Address)
	fmt.Println("}")
}

func CheckParamsExceptApartment(phone, name, city, street, building string) bool { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом
	if Validate(phone, PhoneFlag) && Validate(name, NameFlag) && Validate(city, CityFlag) && Validate(street, StreetFlag) && Validate(building, BuildingOrApartmentFlag) {
		return true
	}
	PrintValidationMessages()
	return false
}

func Validate(data string, flag string) bool {
	switch flag {
	case PhoneFlag:
		return PhoneRegexp.MatchString(data)
	case NameFlag:
		return NameRegexp.MatchString(data)
	case CityFlag:
		return CityRegexp.MatchString(data)
	case StreetFlag:
		return StreetRegexp.MatchString(data)
	case BuildingOrApartmentFlag:
		return BuildingOrApartmentRegexp.MatchString(data)
	default:
		return false
	}
}

func PrintValidationMessages() string {
	fmt.Println("Попробуйте еще раз. При этом введите верные данные")
	fmt.Println("Формат номера телефона +380xxYYYYYYY")
	fmt.Println("Имя - только буквы A-z и/или цифры (от 3 до 15 символов)")
	fmt.Println("Город - только буквы A-z (от 3 до 15 символов)")
	fmt.Println("Улица - только буквы A-z и/или цифры (от 4 до 20 символов)")
	fmt.Println("Дом/квартира - только буквы A-z и/или цифры (от 1 до 5 символов)")
	return "PrintValidationMessages()"
}

func Check(e error) {
	if e != nil {
		panic(e)
	}
}
