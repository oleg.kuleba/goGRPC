// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	mod "gitlab.com/oleg.kuleba/goGRPC/models"
	"gitlab.com/oleg.kuleba/goGRPC/grpcClient/client"
)

// addContactCmd represents the addContact command
var addContactCmd = &cobra.Command{
	Use:   "addContact",
	Short: "addContact phoneNumber name city street building [apartment] - добавляет новый контакт в файл",
	Long: `Команда добавляет новый контакт в файл, требует передачу минимум 5-ти, максимум 6-ти аргументов:
номерТелефона имяВладельца город улица дом [квартира](в указанном порядке)
параметры в квадратных скобках являются опциональными
Например: addContact +380501234567 Anya Dnipro Gagarina 103a 5`,
	Args: cobra.MinimumNArgs(5),
	Run:  addContact,
}

func addContact(cmd *cobra.Command, args []string) {
	if !client.CheckParamsExceptApartment(args[0], args[1], args[2], args[3], args[4]) { // Если аргументы не проходят валидацию (все, кроме квартиры, т.к. она опциональная), выводим инфу об этом и выходим
		return
	}

	// Собираем контакт в структуру
	contact := &mod.Contact{
		Name: args[1],
		Address: &mod.Contact_Address{
			City:     args[2],
			Street:   args[3],
			Building: args[4],
		},
	}

	if len(args) > 5 { // Если квартира указана, то
		if !client.Validate(args[5], client.BuildingOrApartmentFlag) { // валидируем ее. Если не валидно - выходим
			client.PrintValidationMessages()
			return
		}
		contact.Address.Apartment = args[5] // Вписываем в адрес
	}

	// Отдаем на обработку для отправки на сервер
	if flag, msg := client.AddContact(args[0], contact); flag == true {
		fmt.Println("Запись успешно добавлена")
	} else {
		fmt.Println(msg)
	}
}

func init() {
	rootCmd.AddCommand(addContactCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addContactCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addContactCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
