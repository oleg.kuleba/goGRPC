package tests

import (
	mod "gitlab.com/oleg.kuleba/goGRPC/models"
	"gitlab.com/oleg.kuleba/goGRPC/grpcServer/server"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/boltdb/bolt"
	"time"
	"path/filepath"
	"log"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"github.com/golang/protobuf/proto"
	"io"
	"reflect"
)

const (
	address = "localhost:50051"
)

func initTestData() *mod.ContactRequestOrResponse {
	return &mod.ContactRequestOrResponse{
		Phone: &mod.Phone{Number: "+100501234567"},
		Contact: &mod.Contact{
			Name: "Anya",
			Address: &mod.Contact_Address{
				City:      "Dnipro",
				Street:    "Gagarina",
				Building:  "103a",
				Apartment: "5",
			},
		},
	}
}

type dbWorker func(tx *bolt.Tx) error

// Ф-я подготовки работы БД (получение пути к БД, открытие/закрытие, проверка ошибок)
func dbPrepare(fn dbWorker) {
	path, err := filepath.Abs("../grpcServer/phoneBook.db")
	if err != nil {
		log.Fatalf("did not find the path %v. Error: %v", path, err)
	}
	db, err := bolt.Open(path, 0600, &bolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		log.Fatalf("did not open DB %v. Error: %v", server.DBName, err)
	}
	defer db.Close()

	// Открываем Writable транзакцию
	if err := db.Update(fn); err != nil {
		panic(err)
	}
}

// Ф-я наполнения БД тестовыми данными
func fillDB(data *mod.ContactRequestOrResponse) {
	dbPrepare(func(tx *bolt.Tx) error {
		if bucket, err := tx.CreateBucketIfNotExists([]byte(server.ContactsBucketName)); err == nil { // Проверяем наличие баккета (создаем, если нужно)
			if contact := bucket.Get([]byte(data.Phone.Number)); contact == nil { // Проверяем отсутствие номера в БД
				contactBytes, err := proto.Marshal(data.Contact)
				if err != nil {
					log.Fatalf("did not Marshal testVal.Contact. Error: %v", err)
				}
				err = bucket.Put([]byte(data.Phone.Number), contactBytes) // Добавляем запись в БД
			}
		}
		return nil
	})
}

// Ф-я очистка БД от тестовых данных
func clearDB(data *mod.ContactRequestOrResponse) {
	dbPrepare(func(tx *bolt.Tx) error {
		if bucket := tx.Bucket([]byte(server.ContactsBucketName)); bucket != nil { // Проверяем наличие баккета
			if contact := bucket.Get([]byte(data.Phone.Number)); contact != nil { // Проверяем наличие номера в БД
				err := bucket.Delete([]byte(data.Phone.Number)) // Удаляем запись с БД
				if err != nil {
					log.Fatalf("did not Delete testVal. Error: %v", err)
				}
			}
		}
		return nil
	})
}

func TestAddContact(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := mod.NewAddressBookClient(conn)
	testVal := initTestData()

	// Добавление нового контакта
	Convey("AddContact", t, func() {
		res, err := client.AddContact(context.Background(), testVal)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeTrue)
		So(res.Msg, ShouldBeBlank)
	})

	// Попытка добавить существующий контакт
	Convey("AddExistingContact", t, func() {
		res, err := client.AddContact(context.Background(), testVal)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeFalse)
		So(res.Msg, ShouldEqual, server.MsgContactAlreadyExist)
	})

	// Очистка БД от тестовых данных
	clearDB(testVal)
}

func TestFindAll(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := mod.NewAddressBookClient(conn)
	testVal := initTestData()

	// Наполнение БД тестовыми данными
	fillDB(testVal)

	Convey("FindAll", t, func() {
		// Получаем стрим
		stream, err := client.FindAll(context.Background(), &mod.Empty{})
		for {
			// Получаем контакт из стрима
			contact, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("did not get contact from stream. Error: ", err)
			}
			// Проверяем, что тип данных из стрима соответствует типу тестовых данных
			So(contact.Phone, ShouldHaveSameTypeAs, testVal.Phone)
			So(contact.Contact, ShouldHaveSameTypeAs, testVal.Contact)
		}
		So(err, ShouldBeNil)
	})

	// Очистка БД от тестовых данных
	clearDB(testVal)
}

func TestFindByNumber(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := mod.NewAddressBookClient(conn)
	testVal := initTestData()

	// Наполнение БД тестовыми данными
	fillDB(testVal)

	// Поиск контакта по номеру телефона
	Convey("FindByNumber", t, func() {
		res, err := client.FindByNumber(context.Background(), testVal.Phone)
		So(err, ShouldBeNil)
		So(res.Phone.Number, ShouldEqual, testVal.Phone.Number)
		So(reflect.DeepEqual(res.Contact, testVal.Contact), ShouldBeTrue)
	})

	// Очистка БД от тестовых данных
	clearDB(testVal)

	// Попытка найти несуществующий в БД контакт
	Convey("FindByNonExistingNumber", t, func() {
		res, err := client.FindByNumber(context.Background(), testVal.Phone)
		So(err, ShouldBeNil)
		So(res.Phone, ShouldBeNil)
		So(res.Contact, ShouldBeNil)
	})
}

func TestEditContact(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := mod.NewAddressBookClient(conn)
	testVal := initTestData()

	// Наполнение БД тестовыми данными
	fillDB(testVal)

	// Изменим тестовые данные
	testVal = &mod.ContactRequestOrResponse{
		Phone: &mod.Phone{Number: testVal.Phone.Number},
		Contact: &mod.Contact{
			Name: "SomeOtherName",
			Address: &mod.Contact_Address{
				City:     "Kyiv",
				Street:   "Shevchenka",
				Building: "200",
			},
		},
	}

	// Изменение данных о контакте
	Convey("EditContact", t, func() {
		res, err := client.EditContact(context.Background(), testVal)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeTrue)
		So(res.Msg, ShouldBeBlank)
	})

	// Очистка БД от тестовых данных
	clearDB(testVal)

	// Попытка изменить несуществующий в БД контакт
	Convey("EditNonExistingContact", t, func() {
		res, err := client.EditContact(context.Background(), testVal)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeFalse)
		So(res.Msg, ShouldEqual, server.MsgContactNonExist)
	})
}

func TestDeleteContact(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := mod.NewAddressBookClient(conn)
	testVal := initTestData()

	// Наполнение БД тестовыми данными
	fillDB(testVal)

	// Удаление контакта
	Convey("DeleteContact", t, func() {
		res, err := client.DeleteContact(context.Background(), testVal.Phone)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeTrue)
		So(res.Msg, ShouldBeBlank)
	})

	// Попытка удалить несуществующий в БД контакт
	Convey("DeleteNonExistingContact", t, func() {
		res, err := client.DeleteContact(context.Background(), testVal.Phone)
		So(err, ShouldBeNil)
		So(res.IsOk, ShouldBeFalse)
		So(res.Msg, ShouldEqual, server.MsgContactNonExist)
	})

	// Очистка БД от тестовых данных
	clearDB(testVal)
}
